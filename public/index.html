<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="theme-color" content="#000000">
    <link rel="manifest" href="manifest.webmanifest">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.1/leaflet.css">
    <link rel="stylesheet" href="style.css">
    <link rel="shortcut icon" href="images/favicon.png">
    <link rel="apple-touch-icon" href="images/favicon-192.png" />
    <title>Bevis in London</title>

    <meta itemprop="name" content="Bevis in Longon">
    <meta itemprop="description" content="Trace the footsteps of medieval literary hero Bevis of Hampton over modern-day London">
    <meta name="description" content="Trace the footsteps of medieval literary hero Bevis of Hampton over modern-day London">

    <link rel="canonical" href="/">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@dramyburge">
    <meta name="twitter:title" content="Bevis in London">
    <meta name="twitter:description" content="Trace the footsteps of medieval literary hero Bevis of Hampton over modern-day London">
    <meta name="twitter:creator" content="@dramyburge">
    <meta property="og:title" content="Bevis in London">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://bevisinlondon.com/">
    <meta property="og:description" content="Trace the footsteps of medieval literary hero Bevis of Hampton over modern-day London">
    <meta property="og:site_name" content="Bevis in London">
  </head>
  <body>
    <main>
      <h1>Bevis in London</h1>
      <div id="introduction">
        <h2>Introduction</h2>
        <p>Welcome to Bevis in London. This site traces the footsteps of medieval literary hero Bevis of Hampton over modern-day London.</p>

        <h3>Who was Bevis of Hampton?</h3>
        <p>One of the most popular heroic figures of late-fourteenth-century English romance literature was a knight named Bevis of Hampton. A restless wander, exiled from his native Southampton aged only seven, Bevis is raised overseas by a Muslim king, eventually marrying his daughter. On his return, he is thrown into a fierce battle in the narrow streets of medieval London. Today, Bevis is best-known as the imagined founder of the English city of Southampton.</p>

        <h3>What is the connection between Bevis' story and London?</h3>
        <p>Towards the end of the romance, goaded by the King of England’s steward, Bevis is forced into a fierce battle in the City of London, during which many civilians are killed and which results in the marriage of the King’s daughter to Bevis’ son as a means of achieving peace. This scene was added to the English version of the romance and is referred to as the 'London interpolation'.</p>
        <p>Eleven places associated with London geography are named, more than any of the other six Middle English versions of the romance that survive in full.  These are: ‘Londen’; ‘Potenhite’ (Putney); ‘Temse flode’ (river Thames); ‘Westmenster’; ‘Tour street’ (Tower street); ‘Chepe’ (Cheapside); ‘Godes lane’ (Goose lane/Gutter lane); ‘Londegate’ (London gate); ‘Bowe’ (St Mary le Bow church); ‘Londen ston’; and ‘the ledene halle’ (Leadenhall). The romance of Bevis of Hampton is highly unusual in its level of geographical accuracy and detail, suggesting a writer or patron with first-hand knowledge of the city.</p>

        <h3>Why map Bevis in London?</h3>
        <p>While Bevis walked through London over 700 years ago, the way he interacts with London and its citizens encourages us to think about what it meant to be English and to be a hero. The text presents to us ideas about masculinity, violence and migration that are relevant to England in 2018.</p>

        <h3>Tell me more about the site and what I can do with it.</h3>
        <p>This website features a map with location markers from Bevis of Hampton indicating relevant locations from the romance. You can click each of the points of interest and find out more about the romance.</p>
        <p>It also features a walking tour, following Bevis' movements in the City of London. The tour can similarly be followed remotely from your computer. Alternatively, you can follow the route for yourself, downloading the PDF guide.</p>
        <p>This website and the walking tour have been designed by Dr Amy Burge, an academic who has been researching Bevis of Hampton for a decade. The tour points out aspects of the history of medieval London as well as telling the story of the knight Bevis. The message of the tour is that Bevis' London journey reveals much about his character and the social and moral fabric of fourteenth-century England.</p>
        <hr>

        <h3>Find out more</h3>
        <p>You can read a plot summary and find out more about the date and different manuscripts of the romance in the Database of Middle English romance: <a href="https://www.middleenglishromance.org.uk/mer/11">https://www.middleenglishromance.org.uk/mer/11</a></p>
        <p>You can read the romance in its original Middle English on the TEAMS Middle English Texts Series website: <a href="http://d.lib.rochester.edu/teams/text/salisbury-four-romances-of-england-bevis-of-hampton">http://d.lib.rochester.edu/teams/text/salisbury-four-romances-of-england-bevis-of-hampton</a></p>
        <p>Read more from Dr Amy Burge: <a href="https://www.palgrave.com/gb/book/9781137601315">https://www.palgrave.com/gb/book/9781137601315</a></p>
        <p>Technical development by <a href="https://twitter.com/gregtyler">Greg Tyler</a></p>

        <hr>

        <a href="https://gregtyler.co.uk/privacy">Privacy notice</a>
      </div>

      <h2 data-tag="walk">The Bevis in London walk</h2>
      <details class="location location--walk" data-latitude="51.51381" data-longitude="-0.10009">
        <summary>
          <p>Start: Front steps of St Pauls / Ludgate Hill</p>
          <img src="images/st-pauls.jpg" />
        </summary>
        <h3>Start: Front steps of St Pauls / Ludgate Hill</h3>
        <audio src="audio/bevis-in-london-start.mp3" controls class="audio-player"></audio>
        <p>Welcome to the Bevis in London walking tour.</p>
        <p>You’re stood, right now, in front of one of London’s most famous landmarks: St Paul’s Cathedral. The present building, set on the highest point in the City of London, was designed by Sir Christopher Wren and was built between 1675 and 1710, It’s one of the most iconic buildings in London. A cathedral has stood at this site for more than 1400 years – prior to the construction of the current Cathedral stood the medieval cathedral, built between 1087 and 1314 and referred to as Old St Paul’s.</p>
        <p>It’s into the medieval history of the City of London that we now delve, specifically its inclusion in the hugely popular medieval romance Bevis of Hampton. This walking tour traces the route the romance’s hero Bevis took through medieval London layering it over the modern-day City.</p>
        <p>Bevis of Hampton was one of the most popular heroic figures of late medieval English literature and his story continued to be printed and sold into the nineteenth century. Six complete versions of his story survive in Middle English, the earliest of which is in the Auchinleck manuscript which dates from 1330. Today, Bevis is best-known as the imagined founder of the English city of Southampton.</p>
        <p>A restless wanderer, exiled from his native Southampton aged only seven, Bevis was raised overseas by a Muslim king, eventually marrying the King’s daughter. On his return to England, he is thrown into a fierce battle in the narrow streets of medieval London. This scene was newly added to the English versions of the romance.</p>
        <p>Eleven places associated with London geography are named in the Auchinleck version of Bevis, more than any of the other six Middle English versions of the romance that survive. Bevis of Hampton is highly unusual in its level of geographical accuracy and detail. This means that we can map, with some accuracy, Bevis’ movements in London.</p>
        <p>The way Bevis interacts with London is revealing of medieval ways of thinking about Englishness, masculinity, and belonging. This tour, following in Bevis’ footsteps, encourages you to consider what it means to be English and what it means to be a hero in history and today.</p>
        <hr>
        <p>Now walk to the first stop on the tour at the junction of Cheapside and Gutter Lane.</p>
      </details>
      <details class="location location--walk" data-latitude="51.51438" data-longitude="-0.09551">
        <summary>
          <p>Stop 1: Cheapside/Gutter Lane</p>
          <img src='images/gutter-lane.jpg'>
        </summary>
        <h3>Stop 1: Cheapside/Gutter Lane</h3>
        <audio src="audio/bevis-in-london-stop-1.mp3" controls class="audio-player"></audio>
        <p>Please line up on each side of the narrow lane, leaving the bike path clear. This stop gives us an opportunity to talk about the connection between Bevis, London, and commerce.</p>
        <p>The action of the romance is focused on Bevis’s struggles in the narrow lane.</p>
        <blockquote>
          Beves prikede [galloped] forth to Chepe, [Cheapside]<br>
          The folk [crowd] him folwede al to hepe; [as a mob/ moblike]<br>
          Thourgh Godes lane he wolde han flowe, [would have fled]<br>
          Ac [Yet] sone within a lite throwe [in a short time]<br>
          He was beset in bothe side,<br>
          That fle ne mighte he nought that tide. [He could not flee at that time]<br>
          […]<br>
          That lane was so narw ywrought, [narrowly made]<br>
          That he mighte defende him nought,<br>
          He ne [Nor] Arondel, is stede,<br>
          Ne mighte him terne for non nede.<br>
          […]<br>
          Out of the lane a [he] wolde ten, [go]<br>
          The chynes [chains] held him faste aghen [tightly].<br>
          With is swerd he smot the chayne,<br>
          That hit fel a peces twayne, [two pieces]<br>
          And forth a [he] wente in to Chepe;<br>
          […]<br>
          Now beginneth the grete bataile<br>
          Of Sire Beves, withouten faile,<br>
          That he dede [did] ayenes [against] that cité.<br>
        </blockquote>
        <p>This stop is where Bevis’ battle with the citizens of London really begins. The romance tells us that Bevis rode towards Cheapside, with the citizens following him in a rabble. He would have ridden through ‘Godes Lane’ but he was quickly beset on both sides and could not flee. The lane is so narrow that he can’t defend himself or even turn around on his horse. The citizens raise chains to hold Bevis in the lane, but he manages to break them with his sword and escape into Cheapside.</p>
        <p>Whether the medieval author meant ‘Godes Lane’ to indicate Gutter Lane, Goose Lane (a narrow alley that used to run from Cordwainer Street (now Bow lane) to Cheapside) or Bread Street (given in one manuscript), it’s clear that the street referred to was narrow and led onto Cheapside, like this one.</p>
        <p>Late thirteenth- and early fourteenth-century London was an increasingly important mercantile centre, with Cheapside at the heart of the booming trade economy. Indeed, this period recorded the highest demand for shops and the population density was greater than any time until the start of the seventeenth century. London was, along with Southampton, one of the busiest ports in southern England.</p>
        <p>It’s significant that this is the area of London in which Bevis is attacked; the streets of mercantile Cheapside are an appropriate destination for the boy who was himself traded at age seven. The romance is filled with mercantile geography with references to European trade routes and travel between them. Indeed, Bevis himself follows many of these routes during the romance.</p>
        <p>The mercantile and trade connotations of Cheapside are symbolically aligned with the overall themes of the romance, and support the idea of Bevis himself as a trade item. In short, because Bevis is himself so concerned with trade – both its routes and actions – the London interpolation is an extension of that theme evident elsewhere in the romance.</p>
        <hr>
        <p>Now walk along Cheapside to Stop 2, outside Bank underground station.</p>
      </details>
      <details class="location location--walk" data-latitude="51.51345" data-longitude="-0.08849">
        <summary>
          <p>Stop 2: Lombard Street (outside Bank underground station)</p>
          <img src='images/bank.jpg'>
        </summary>
        <h3>Stop 2: Lombard Street (outside Bank underground station)</h3>
        <audio src="audio/bevis-in-london-stop-2.mp3" controls class="audio-player"></audio>
        <p>During the battle between Bevis and the citizens of London a new villain enters – a Lombard, or Italian merchant. The romance tells us:</p>
        <blockquote>
          Thar was a Lombard in the toun,<br>
          That was scherewed [wicked] and feloun [villainous];<br>
          He armede him in yrene wede [armour]<br>
          And lep [leapt] upon a sterne [strong] stede<br>
          And rod forth with gret randoun [violence]<br>
          And thoughte have slawe [aimed to kill] Sire Bevoun.<br>
          <br>
          With an uge masnel [huge club]<br>
          Beves a hite [he hit] on the helm[et] of stel,<br>
          That Beves of Hamtoun, veraiment, [truly]<br>
          Was astoned [dazed] of [from] the dent;<br>
          […]<br>
          Thanne com priken [galloping] is [his] sone Gii,<br>
          To helpe his fader, hastely;<br>
          With a swerd drawe [drawn] in [h]is hond<br>
          To that Lombard sone a wond [he quickly proceeded]<br>
          And smot him so upon the croun, [head]<br>
          That man and hors he clevede [clove] doun; [down]
        </blockquote>
        <p>Merchants from Lombardy were subject to significant hostility in medieval London as a visible foreign community that was centred around Lombard Street, just to our left. Following the expansion of the English wool trade in the first half of fourteenth century, Italian traders had become key competitors for groups of English merchants who wanted to manufacture the wool themselves. Lombards had equally managed to establish themselves in the new area of banking, lending money and credit.</p>
        <p>Lombards were labelled traitors and spies. Just as in Bevis, street brawls were not uncommon between Londoners and foreigners, who were killed in rioting between 1359-1369, and in 1381, 1456 and 1457. There are scathing references to Lombards in other late medieval literature, including Chaucer’s Canterbury Tales, the popular medieval romance Guy of Warwick, and Gower’s Confessio Amantis and ‘Mirour de l'Omme’.</p>
        <p>A Lombard in medieval London thus indicated an enemy, a commercial rival, and a foreigner. The specific reference to a Lombard in the romance underscores the presentation of London as multicultural – as a commercial area – and Bevis’ identity as a foreigner in a city populated by other foreigners.</p>

        <hr>
        <p>Now walk down Lombard Street and St Swithin’s Lane to Stop 3 on the tour.</p>

        <h4>Further reading on Lombards in London</h4>
        <ul>
          <li>Alice Beardwood, Alien Merchants in England, 1350 to 1377 (Cambridge, Massachusetts, 1931)</li>
          <li>Derek Pearsall, “Strangers in Late-Fourteenth-Century London” in The Stranger in Medieval Society, edited by F. R. P. Akehurst and Stephanie Cain Van D’Elden (Minneapolis: University of Minnesota Press, 1997), pp. 46-62</li>
          <li>Claire Sponsler, “Alien Nation: London’s aliens and Lydgate’s mummings for the mercers and goldsmiths” in The Postcolonial Middle Ages, edited by Jeffrey Jerome Cohen (London: Palgrave, 2000), pp. 229-242</li>
        </ul>
      </details>
      <details class="location location--walk" data-latitude="51.51147" data-longitude="-0.08952">
        <summary>
          <p>Stop 3: The London Stone, 111 Cannon Street</p>
          <img src='images/london-stone.jpg'>
        </summary>
        <h3>Stop 3: The London Stone, 111 Cannon Street</h3>
        <audio src="audio/bevis-in-london-stop-3.mp3" controls class="audio-player"></audio>
        <p>On this spot, stood the London Stone – a landmark referenced during Bevis’ battle in London. It’s currently housed in the Museum of London but will be returning to this spot once the building it is housed in has been redeveloped. The London Stone is a location marker referred to in documents as early as 1098. It’s been moved a few times in its history but it is firmly ensconced in the literary and mythical history of London.</p>
        <p>It’s been suggested that the London Stone functioned as a distance marker – in his Dictionary of London (1917) London historian Henry Harben notes that the stone is often mentioned in local records to mark boundaries between houses. It has been referred to as ‘a “Milliarium” or milestone’ from which English roads radiated and from which distances were marked, similar to the stone in the forum at Rome, although there is no evidence that the Romans were the ones who placed the Stone.</p>
        <p>The London Stone’s function of measuring distance is particularly relevant to Bevis of Hampton as the romance is unusually concerned with time, distance, and travel. Bevis expands its source material to add 46 new locations in Europe, Asia, and Africa – two major new scenes are a dragon fight in Germany, and the London fight sequence. The geographical range of the romance is thus significantly expanded when it is reworked in English to a total of 63 geographic locations. Yet the change is not merely quantitative, but also qualitative. In addition to mentioning more places, the Middle English Bevis provides specific route details, outlining journey times and methods of travel. It seems appropriate, therefore, for a romance so concerned with routes, places and the distances between them, to refer to a landmark whose function is to measure distance.</p>

        <h4>Find out more about the London Stone</h4>
        <ul>
          <li><a href="https://www.museumoflondon.org.uk/discover/london-stone-seven-strange-myths">John Clark, ‘London Stone in seven strange myths’, Museum of London 4 March 2016</a></li>
          <li>John Clark, “London Stone: Stone of Brutus or Fetish Stone – making the Myth”, Folklore 121.1 (2010): 38-60</li>
          <li>Henry Harben, A Dictionary of London (1917). <a href="http://www.british-history.ac.uk/no-series/dictionary-of-london">Available at British History Online</a></li>
      </details>
      <details class="location location--walk" data-latitude="51.51271" data-longitude="-0.09361">
        <summary>
          <p>Stop 4 (final stop): Bow Lane</p>
          <img src='images/bow-lane.jpg'>
        </summary>
        <h3>Stop 4 (final stop): Bow Lane</h3>
        <audio src="audio/bevis-in-london-stop-4.mp3" controls class="audio-player"></audio>
        <p>After Bevis has been fighting for a time, his sons rush to join the battle to avenge their father. However, their mode of entry – specifically their choice of steed – is unusual. Guy choose a ‘rabit’ – an Arabian horse – and his brother Miles rides into London on a ‘dromedary’ – a camel. This is the only time these two animals are mentioned in the entire romance.</p>
        <blockquote>
          Sire Gii lep on a rabit, [Arabian horse]<br>
          That was meche [large] and nothing lite, [small]<br>
          And tok a spere in is hond,<br>
          Out at the halle dore a wond<br>
          Toward the cité of Londen toun,<br>
          And Sire Miles with gret randoun [violence]<br>
          Lep upon a dromedary, [camel]<br>
          To prike [spur] wolde he nought spary, [spare]<br>
          Whan thai come to Londen gate,<br>
          Mani man thai fonde ther-ate,<br>
          Wel iarmed to the teth,<br>
          So the Frensche bok us seth,<br>
        </blockquote>

        <p>Bevis is already well-established as an outsider by this point. The entry of his two sons by his Muslim-born wife riding such comedically alien animals merely serves to emphasise the family’s migrant status – the extent to which they don’t belong. Each time Bevis returns to England, in particular London, he becomes an unwelcome enemy.</p>
        <p>Constantly traveling, Bevis is defined by his movement between geographical locations; the romance is not interested in him when he is in one place, but insists that he keep moving.  This means that he is never truly comfortable either in the land of his birth, England, or in the Muslim east where he was raised.</p>
        <p>Ultimately, Bevis is a migrant who struggles to find a place to belong. Stripped of his home and possessions as a child, raised overseas in a Muslim family, he is dependent on the English authorities to grant him his heritage and right to remain in England. The English crown could threaten his status at any point. This 700-year-old romance thus gives us an example of a medieval migrant whose anxieties and experiences could not be more current.</p>
      </details>

      <h2 data-tag="bevis">Other Bevis points of interest in London</h2>
      <section class="location location--bevis" data-latitude="51.4625524" data-longitude="-0.2167462">
        <h2>‘Potenhite’ (Putney)</h2>
        Anon the knight, Sire Bevoun,<br>
        […] And toward Londen a wente swithe;<br>
        His quene a let at <strong>Potenhithe</strong>; (lines 4288-4290)
      </section>
      <section class="location location--bevis" data-latitude="51.4636" data-longitude="-0.1990">
        <h2>‘Temse flode’ (river Thames)</h2>
        He tok with him sex knightes<br>
        And wente forth anon rightes,<br>
        And in is wei forth a yode<br>
        And pasede over <strong>Temse flode</strong>. (lines 4291-4)
      </section>
      <section class="location location--bevis" data-latitude="51.4965" data-longitude="-0.1408">
        <h2>‘Westmenster’</h2>
        To <strong>Westmenster</strong> whan he com than,<br>
        A fond the king and mani man,<br>
        And on is knes he him set,<br>
        The king wel hendeliche a gret<br>
        And bad before his barnage,<br>
        That he him graunte is eritage. (lines 4925-4300)<br>
        <br>
        [The king agrees to give Bevis his birth right but a treacherous steward accuses Bevis of killing the king’s son and incites the citizens to fight against Bevis]
      </section>
      <section class="location location--bevis" data-latitude="51.51016" data-longitude="-0.08190">
        <h2>‘Tour street’ (Tower street)</h2>
        Beves that herde, a was wroth,<br>
        And lep to hors, withouten oth,<br>
        And rod to Londen, that cité,<br>
        With sex knightes in meiné.<br>
        Whan that he to Londen cam,<br>
        In <strong>Tour strete</strong> is in he nam<br>
        And to the mete he gan gon,<br>
        And is knightes everichon. (4315-22)<br>
        <br>
        [The people of London arm themselves against Bevis. Bevis is informed that the citizens are hunting him and arms himself. Bevis and the traitorous steward clash and Bevis kills him. The citizens then attack Bevis.]
      </section>
      <section class="location location--bevis" data-latitude="51.51368" data-longitude="-0.09174">
        <h2>‘Chepe’ (Cheapside)</h2>
        Beves prikede forth to <strong>Chepe</strong>,<br>
        The folk him folwede al to hepe; (4395-6)
      </section>
      <section class="location location--bevis" data-latitude="51.51494" data-longitude="-0.09540">
        <h2>‘Godes lane’ (probably Gutter lane)</h2>
        Thourgh <strong>Godes lane</strong> he wolde han flowe,<br>
        Ac sone within a lite throwe<br>
        He was beset in bothe side,<br>
        That fle ne mighte he nought that tide. (4397-4400)<br>
        [Bevis’ knights are killed and he tries to flee]<br>
        That lane was so narw ywrought,<br>
        That he mighte defende him nought,<br>
        He ne Arondel, is stede,<br>
        Ne mighte him terne for non nede. (4411-14)<br>
        [Bevis prays]<br>
        Out of the lane a wolde ten,<br>
        The chynes held him faste aghen.<br>
        With is swerd he smot the chayne,<br>
        That hit fel a peces twayne,<br>
        And forth a wente in to Chepe; (4419-23)<br>
        <br>
        [the citizens chase Bevis]<br>
        <br>
        Now beginneth the grete bataile<br>
        Of Sire Beves, withouten faile,<br>
        That he dede ayenes that cité. (4433-5)
      </section>
      <section class="location location--bevis" data-latitude="51.51393" data-longitude="-0.10212">
        <h2>‘Londegate’ (London gate)</h2>
        [The people of London attack Bevis – he kills 5000 of them in the course of the fight. Josian hears that Bevis has been killed. She asks her sons to avenge their father. They ride into London on an Arabian horse and a camel.]<br>
        Whan thai come to <strong>Londen gate</strong>,<br>
        Mani man thai fonde ther-ate,<br>
        Wel iarmed to the teth,<br>
        So the Frensche bok us seth, (4483-6)<br>
        [The sons fight]
      </section>
      <section class="location location--bevis" data-latitude="51.51372" data-longitude="-0.09354">
        <h2>‘Bowe’ (St Mary le Bow church)</h2>
        Tho began a gret bataile<br>
        Betwene <strong>Bowe</strong> and Londen ston,<br>
        That time stod us never on. (4494-6)
      </section>
      <section class="location location--bevis" data-latitude="51.51145" data-longitude="-0.08942">
        <h2>‘Londen ston’ (London Stone)</h2>
        Tho began a gret bataile<br>
        Betwene Bowe and <strong>Londen ston</strong>,<br>
        That time stod us never on. (4494-6)
      </section>
      <section class="location location--bevis" data-latitude="51.51272" data-longitude="-0.08340">
        <h2>‘The ledene halle’  (Leadenhall)</h2>
        So meche folk was slawe and ded,<br>
        That al Temse was blod red;<br>
        The nombre was, veraiment,<br>
        To and thretti thosent.<br>
        And also sone as hit was night,<br>
        To <strong>the ledene halle</strong> thai wente right;<br>
        A fette Josian with faire meiné<br>
        To Londen, to that riche cité,<br>
        And held a feste fourtene night<br>
        To al that ever come, aplight! (4529-38)<br>
        [King Edgar hears about the fighting and mourns the loss of 32,000 citizens. He proposes Bevis’ son marry his daughter to broker peace]
      </section>

      <div id="feedback">
        <h2>Feedback</h2>
        <p><strong>Email:</strong> <a href="mailto:a.burge@bham.ac.uk">a.burge@bham.ac.uk</a></p>
        <p>This is a new tool and new content and features will be added along the way. If you've got any suggestions for content you'd like to see or any ideas for improving the site let us know!</p>
      </div>
    </main>

    <div id="london_map" hidden></div>

    <div hidden>
      <svg id="map-marker" viewBox="0 0 24 24" style="filter: drop-shadow(0 1px 2px hsla(0, 0%, 0%, 0.5));">
        <g fill="currentColor">
          <circle cx="12" cy="12" r="6"></circle>
          <path d="M6 12L18 12L12 24Z"></path>
          <text x="12" y="12" dy="3" style="font-size:.4rem" fill="hsla(0, 0%, 100%, 0.9)" text-anchor="middle" data-bind="counter"></text>
        </g>
      </svg>
    </div>

    <div class="modal__backdrop" hidden>
      <div class="modal">
        <div class="modal__header">
          <h2 class="modal__title">Title</h2>
          <button class="modal__dismiss">&times;</button>
        </div>
        <div class="modal__content"></div>
      </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.1/leaflet.js" charset="utf-8"></script>
    <script src="map.js" charset="utf-8"></script>
    <script data-goatcounter="https://bevisinlondon.goatcounter.com/count" async src="//gc.zgo.at/count.js"></script>
  </body>
</html>
