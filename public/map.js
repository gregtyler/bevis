// Hide the basic HTML contents
document.querySelector('main').hidden = true;

var _paq = _paq || [];

// Waypoints for the walking route
var waypoints = [
  [51.51381, -0.10009], //st paul's
  [51.51406, -0.09960],
  [51.51424, -0.09866],
  [51.51429, -0.09864],
  [51.51440, -0.09699],
  [51.51443, -0.09681],
  [51.51454, -0.09673],
  [51.51454, -0.09663],
  [51.51460, -0.09648],
  [51.51438, -0.09551], //gutter lane
  [51.51381, -0.09234],
  [51.51337, -0.08951],
  [51.51345, -0.08849], //lombard street/bank
  [51.51326, -0.08883],
  [51.51275, -0.08832], //st swithin's lane
  [51.51143, -0.08935],
  [51.51147, -0.08952], //london stone
  [51.51214, -0.09271],
  [51.51247, -0.09374],
  [51.51271, -0.09361] // bow lane
];

var modal = (function() {
  var $backdrop = document.querySelector('.modal__backdrop');
  var $container = document.querySelector('.modal__container');
  var $content = document.querySelector('.modal__content');
  var $title = document.querySelector('.modal__title');
  var $dismiss = document.querySelector('.modal__dismiss');

  function hide() {
    // Stop playing any audio on the modal
    const $audios = Array.from($content.querySelectorAll('audio'))
    $audios.forEach($audio => {
      $audio.pause();
      $audio.currentTime = 0;
    });

    $backdrop.hidden = true;
  }

  document.body.addEventListener('keydown', function(e) {
    if (e.which === 27) hide();
  });
  $dismiss.addEventListener('click', hide);
  $backdrop.addEventListener('click', function(e) {
    if (e.target === $backdrop) hide();
  });

  return function modal($el) {
    var $elClone = $el.cloneNode(true);

    // Remove summary if present
    var $summary = $elClone.querySelector('summary');
    if ($el.tagName === 'DETAILS' && $summary) {
      $elClone.removeChild($summary);
    }

    // Add title if specified
    var $leadTitle = $elClone.querySelectorAll('h2, h3, h4')[0];
    if ($leadTitle) {
      $title.innerHTML = $leadTitle.innerHTML;
      $elClone.removeChild($leadTitle);
    } else {
      $title.innerHTML = '';
    }

    // Populate the modal
    $content.innerHTML = $elClone.innerHTML;

    // Show the modal and backdrop
    $backdrop.hidden = false;

    // Move focus to dismiss button
    $dismiss.focus();
  }
})();

// Create points from HTML data
var points = [];
document.querySelectorAll('.location').forEach(function($location) {
  var $locationClone = $location.cloneNode(true);
  var $summary = $locationClone.querySelector('summary');
  if ($summary) {
    $locationClone.removeChild($summary);
  }

  points.push({
    lat: parseFloat($location.dataset.latitude, 10),
    lng: parseFloat($location.dataset.longitude, 10),
    text: $summary ? $summary.innerHTML : $locationClone.innerHTML,
    $modal: $summary ? $locationClone : null,
    tags: Array.from($location.classList).filter(c => c.substr(0, 10) === 'location--').map(c => c.substr(10)),
    isVisible: true
  });
});

// Define map
document.querySelector('#london_map').hidden = false;
var map = L.map('london_map');
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    tileSize: 512,
    maxZoom: 18,
    zoomOffset: -1,
    id: 'mapbox/streets-v11',
    accessToken: 'pk.eyJ1IjoiZHJhbXlidXJnZSIsImEiOiJjamRhb3dmanYycHR5MndyMjFxMmJqNTduIn0.171CpcQLFeb-Jg6QwxWpjw'
}).addTo(map);

function generateIcon(style, counter) {
  var $icon = document.querySelector('#map-marker').cloneNode(true);
  $icon.querySelector('[data-bind="counter"]').innerHTML = counter;
  $icon.classList.add('marker-icon--' + style);
  return L.divIcon({html: $icon.outerHTML, iconSize: [50, 50], iconAnchor: [25, 50], popupAnchor: [0, -35]});
}

// Put points on map
var counters = {};
points.forEach(function(point) {
  var tag = point.tags[0];
  if (typeof counters[tag] === 'number') {
    counters[tag]++;
  } else {
    counters[tag] = 1;
  }

  point.marker = L.marker([point.lat, point.lng], {icon: generateIcon(tag, counters[tag])}).addTo(map);

  // Show a popup on click
  var $div = document.createElement('div');
  $div.innerHTML = point.text;
  $div.classList.add('marker-container');

  if (point.$modal) {
    $div.classList.add('marker-container--linked');
    $div.addEventListener('click', function() {
      modal(point.$modal);
    });
  }

  point.marker.bindPopup($div, {closeButton: false});
});

L.polyline(waypoints, {color: '#1494c1', dashArray: '5,10', weight: 3.5, opacity: 0.8}).addTo(map);

// Get saved tag visibility
const tagVisibilityJSON = localStorage.getItem('tags');
var tagVisibility = {};
if (tagVisibilityJSON !== null) {
  tagVisibility = JSON.parse(tagVisibilityJSON);
}

// Load up tag data
var tags = points
  .reduce((prev, p) => prev.concat(p.tags), [])
  .filter((tag, ind, arr) => arr.indexOf(tag) === ind)
  .map(function(tag) {
    return {
      code: tag,
      label: document.querySelector('[data-tag="' + tag + '"]').innerText,
      isVisible: typeof tagVisibility[tag] === 'boolean' ? tagVisibility[tag] : true
    };
  });

// Add options menu
var $toolbar = document.createElement('div');
$toolbar.classList.add('toolbar');

tags.forEach(function(tag) {
  var $checkbox = document.createElement('input');
  $checkbox.type = 'checkbox';
  $checkbox.checked = tag.isVisible;
  $checkbox.addEventListener('change', () => {
    tag.isVisible = $checkbox.checked;

    // Save tag visibility
    tags.forEach(function(tag) {
      tagVisibility[tag.code] = tag.isVisible;
    });
    localStorage.setItem('tags', JSON.stringify(tagVisibility));

    // Redraw map with shown/hidden markers
    mapRedraw();
  });
  var $label = document.createElement('label');
  $label.classList.add('toolbar__control');
  $label.appendChild($checkbox);
  $label.appendChild(document.createTextNode(tag.label));
  $toolbar.appendChild($label);
});

$links = document.createElement('div');
$links.classList.add('toolbar__links');

// Add about link
var $introLink = document.createElement('a');
$introLink.href = '#';
$introLink.innerHTML = 'About';
$introLink.addEventListener('click', function(e) {
  e.preventDefault();
  modal(document.getElementById('introduction'));
});
$links.append($introLink);

// Add feedback link
var $feedbackLink = document.createElement('a');
$feedbackLink.href = '#';
$feedbackLink.innerHTML = 'Feedback';
$feedbackLink.addEventListener('click', function(e) {
  e.preventDefault();
  modal(document.getElementById('feedback'));
});
$links.append($feedbackLink);

$toolbar.append($links);

document.body.append($toolbar);

// Redraw markers and resize map based on data changes
function mapRedraw() {
  // Show/hide markers based on point data
  points.forEach(function(point) {
    point.isVisible = false;
    point.tags.forEach(tag => {
      if (tags.find(t => t.code === tag).isVisible) {
        point.isVisible = true;
      }
    });

    point.marker.setOpacity(point.isVisible ? 1 : 0);
    point.marker.setZIndexOffset(point.isVisible ? 5 : 0)
  });

  // Fit the new bounds
  const visiblePoints = points.filter(p => p.isVisible);

  if (visiblePoints.length) {
    latMin = Math.min.apply(window, visiblePoints.map(p => p.lat));
    lngMin = Math.min.apply(window, visiblePoints.map(p => p.lng));
    latMax = Math.max.apply(window, visiblePoints.map(p => p.lat));
    lngMax = Math.max.apply(window, visiblePoints.map(p => p.lng));
  } else {
    latMin = 51.507;
    lngMin = -0.1;
    latMax = 51.52;
    lngMax = -0.085;
  }

  map.fitBounds([
    [latMin, lngMin],
    [latMax, lngMax]
  ], {padding: [20, 20]});
}

// Redraw with a slight delay and throttle rate at which it can fire
var resizeTimeout;
function throttledRedraw() {
  if (!resizeTimeout) {
    resizeTimeout = setTimeout(function() {
      resizeTimeout = null;
      mapRedraw();
    }, 66);
  }
}

window.addEventListener('resize', throttledRedraw);

// Draw the map
mapRedraw();

// Show introductory modal if we haven't already
var introductionShown = localStorage.getItem('introduction-shown');
if (introductionShown === null) {
  modal(document.getElementById('introduction'));
  localStorage.setItem('introduction-shown', 'true');
}
